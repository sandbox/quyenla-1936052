<div class="content-time">
  <div id="countrytime"><?php print $timezone ?></div>  
  <div id="timeWrapper">
    <div class="widgetHeading"><?php print $title ?></div>
    <div id="time">
      <span id="hours1" class="digit_0"></span>
      <span id="hours2" class="digit_0"></span>
      <span class="colon" id="colon"></span>
      <span id="minutes1" class="digit_0"></span>
      <span id="minutes2" class="digit_0"></span>
    </div>
  </div>
</div>
