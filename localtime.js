
function setTime(){
  var value="";
  try{value=$("#countrytime").html()}
  catch(err){return};
  if(value==null||value.length==0)return;
  calcTime(value);
  setTimeout("setTime()",1000);
}setTimeout("setTime()",1000);
function calcTime(value){
  sign="+";
  if(value.substring(0,1)=="-")sign="-";
  var hourpart=value.substring(1,value.length-2);
  if(/^0/.test(hourpart)){var hours=parseInt(hourpart.substring(1,2))}
  else var hours=parseInt(hourpart);
  var minpart=value.substring(value.length-2,value.length);
  if(/^0/.test(minpart)){
    var minutes=parseInt(minpart.substring(1,2))
  }else var minutes=parseInt(minpart);
  if(isNaN(hours))hours=0;
  if(isNaN(minutes))minutes=0;
  var date=new Date();
  if(sign=="-"){
    date.setMinutes(date.getUTCMinutes()-minutes);
    date.setHours(date.getUTCHours()-hours)
  }else{
    date.setMinutes(date.getUTCMinutes()+minutes);
    date.setHours(date.getUTCHours()+hours)
  };
  var today=new Date(Date.UTC(date.getUTCFullYear(),date.getUTCMonth(),date.getUTCDate(),date.getHours(),date.getMinutes(),date.getUTCSeconds())),currentHour=date.getHours().toString();
  if(currentHour.length==1)currentHour=0+currentHour;
  var currentMinute=date.getMinutes().toString();
  if(currentMinute.length==1)currentMinute=0+currentMinute;
  $("#hours1").attr('class',"digit_"+currentHour.substring(0,1));
  $("#hours2").attr('class',"digit_"+currentHour.substring(1,2));
  $("#minutes1").attr('class',"digit_"+currentMinute.substring(0,1));
  $("#minutes2").attr('class',"digit_"+currentMinute.substring(1,2))
}
